#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "SVCDebug.hpp"

Handle SVCDebug::debugHandle = 0;
u64 SVCDebug::baseAddress = 0;

u32 SVCDebug::currentProcess = 0;

u32 SVCDebug::ProcessInfo::totalProcesses = 0;
u64 *SVCDebug::ProcessInfo::pidList = new u64[MAX_PROCESS_NUMBER];
u64 *SVCDebug::ProcessInfo::tidList = new u64[MAX_PROCESS_NUMBER];

u32 SVCDebug::MenuKeyInteract() {
	while(true) {
		hidScanInput();
		u32 kDown = hidKeysDown(CONTROLLER_P1_AUTO);

		if(kDown & KEY_A) {
			free(ProcessInfo::pidList);
			free(ProcessInfo::tidList);
			return KEY_A;
		}

		else if(kDown & KEY_B)
			return KEY_B;

		else if(kDown & KEY_DUP) {
			if(currentProcess > 0) --currentProcess;
			SVCDebug::DisplayMenu();
		}

		else if(kDown & KEY_DDOWN) {
			if(currentProcess < ProcessInfo::totalProcesses - 2) ++currentProcess;
			SVCDebug::DisplayMenu();
		}
	}
}

void SVCDebug::DisplayMenu() {
	consoleClear();
	puts("Process Select: DPadUp Button or DPadDown Button\r\nEnter: A Button\r\nExit: B button\r\n");
	printf("Memory indexed number: %04d\r\n", currentProcess);
	printf("PID: %lX\r\n", ProcessInfo::pidList[currentProcess]);
	printf("TID: %lX\r\n", ProcessInfo::tidList[currentProcess]);
	printf("\r\n\r\n");
}

int SVCDebug::GetProcessListInfo(int processLimit) {
	memset(SVCDebug::ProcessInfo::pidList, 0, processLimit);
	memset(SVCDebug::ProcessInfo::tidList, 0, processLimit);

	svcGetProcessList(&ProcessInfo::totalProcesses, ProcessInfo::pidList, processLimit); // *num_out, *pid_out, max_pids

	while(currentProcess < ProcessInfo::totalProcesses)
		pminfoGetTitleId(ProcessInfo::tidList + currentProcess, ProcessInfo::pidList[currentProcess++]);

	currentProcess -= 2;

	return ProcessInfo::totalProcesses;
}

void SVCDebug::ErrorOut(const char *s, Result r) {
	printf("Error! Syscall %s returned 0x%08X.\r\n", s, r);
}

bool SVCDebug::AttachFindMemreg() {
	consoleClear();
	printf("Running syscall svcDebugActiveProcess on process %lu.\r\n", SVCDebug::ProcessInfo::pidList[currentProcess]);
	Result r = svcDebugActiveProcess(&debugHandle, ProcessInfo::pidList[currentProcess]);

	if(R_FAILED(r)) {
		ErrorOut("svcDebugActiveProcess", r);
		return false;
	}

	MemoryInfo meminfo{0};
	u32 pageinfo;

	if(R_FAILED(r = svcQueryDebugProcessMemory(&meminfo, &pageinfo, debugHandle, 0))) { // Start region => address 0 (last arg).
		ErrorOut("svcQueryDebugProcessMemory", r);
		return false;
	}

	baseAddress = meminfo.size; // Skip Memory_Unmapped first region.

	while(true) {
		if(baseAddress > 0x9000000000000000)
			return false;

		if(R_FAILED(r = svcQueryDebugProcessMemory(&meminfo, &pageinfo, debugHandle, baseAddress))) {
			ErrorOut("svcQueryDebugProcessMemory", r);
			return false;
		}

		u64 nextBaseAddress = meminfo.addr + meminfo.size;

		// The svcQueryDebugProcessMemory will return a base and size that add up to overflow the 64-bit field and return to zero for address spaces outside of range.
		if(!nextBaseAddress) {
			puts("The main memory region was not found.");
			return false;
		}

		static u8 plausibleReg = 0;

		if(meminfo.type == MemType_CodeStatic && meminfo.perm == Perm_Rx) { // The 2nd ASM region with CHMOD 101 (R-X) is always our main code.
			if(plausibleReg == 1)
				return true;
			
			if(!plausibleReg) {
				baseAddress = nextBaseAddress; // Move to next region.
				++plausibleReg;
				continue;
			}

			else {
				puts("The main memory region was not found.");
				return false;								
			}
		}

		baseAddress = nextBaseAddress; // Move to next region.
	}
}