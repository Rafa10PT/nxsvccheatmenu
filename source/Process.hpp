#ifndef PROCESS_HPP
#define PROCESS_HPP

#include "SVCDebug.hpp"
#include "CMenu.hpp"

class Process {
	public:
		static void Write64(u32 address, u64 value);
		static void Write32(u32 address, u32 value);
		static void Write16(u32 address, u16 value);
		static void Write8(u32 address, u8 value);
		static void Read64(u32 address, u64 &value);
		static void Read32(u32 address, u32 &value);
		static void Read16(u32 address, u16 &value);
		static void Read8(u32 address, u8 &value);
		static CMenu *menu;
};

#endif