#include "cheats.hpp"

u64 lsl_vr_limiter_value = 0;

void vr_limiter_setter() {
	u16 offset = 1;

	while(true) {
		std::cout << "VR Limiter Value: " << lsl_vr_limiter_value << std::endl;
		std::cout << "Offset: " << offset << std::endl;
		puts("Press A to apply.");

		while(true) {
			hidScanInput();
			u32 kDown = hidKeysDown(CONTROLLER_P1_AUTO);

			if(kDown & KEY_DUP) {
				lsl_vr_limiter_value += offset;
				if(lsl_vr_limiter_value > 0xFFF) lsl_vr_limiter_value = 0xFFF;
				break;
			}

			if(kDown & KEY_DDOWN) {
				lsl_vr_limiter_value -= offset;
				if((s16)lsl_vr_limiter_value < 0) lsl_vr_limiter_value = 0;
				break;
			}

			if(kDown & KEY_DLEFT) {
				if(offset < 1000) offset *= 10;
				break;
			}

			if(kDown & KEY_DRIGHT) {
				if(offset != 1) offset /= 10;
				break;
			}

			if(kDown & KEY_A) {
				consoleClear();
				lsl_vr_limiter_value <<= 32;
				return;
			}
		}

		consoleClear();
	}
}

void vr_limiter(MenuEntry *entry) {
	// v1.6.0: Process::Write64(0x47E058, 0xE3A00000E1A00000 | lsl_vr_limiter_value);
	Process::Write64(0x482A84, 0xE3A00000E1A00000 | lsl_vr_limiter_value);

	if(!entry->IsActivated()) Process::Write64(0x47E058, 0xE3400001E308069F);
}