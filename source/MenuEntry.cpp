#include "MenuEntry.hpp"

MenuEntry::MenuEntry(const std::string &name, FuncPointer function) : entryName{name}, entryFunction{function}, entryMenuFunction{nullptr} {}
MenuEntry::MenuEntry(const std::string &name, FuncPointer function, MenuFuncPointer menuFunction) : entryName{name}, entryFunction{function}, entryMenuFunction{menuFunction} {}

void MenuEntry::ChangeState() {
	isActivated = !isActivated;
}

void MenuEntry::Enable() {
	isActivated = true;
}

void MenuEntry::Disable() {
	isActivated = false;
}

void MenuEntry::SetWasActivatedStatus() {
	wasActivated = true;
}

void MenuEntry::RemoveWasActivatedStatus() {
	wasActivated = false;
}

bool MenuEntry::IsActivated() {
	return isActivated;
}

bool MenuEntry::WasActivated() {
	return wasActivated;
}