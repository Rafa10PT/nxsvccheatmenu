#include "Process.hpp"

CMenu *Process::menu = nullptr;

void Process::Write64(u32 address, u64 value) {
	if(R_FAILED(svcWriteDebugProcessMemory(SVCDebug::debugHandle, &value, SVCDebug::baseAddress + address, 8)))
		menu->SetErrorFlag();
}

void Process::Write32(u32 address, u32 value) {
	if(R_FAILED(svcWriteDebugProcessMemory(SVCDebug::debugHandle, &value, SVCDebug::baseAddress + address, 4)))
		menu->SetErrorFlag();
}

void Process::Write16(u32 address, u16 value) {
	if(R_FAILED(svcWriteDebugProcessMemory(SVCDebug::debugHandle, &value, SVCDebug::baseAddress + address, 2)))
		menu->SetErrorFlag();
}

void Process::Write8(u32 address, u8 value) {
	if(R_FAILED(svcWriteDebugProcessMemory(SVCDebug::debugHandle, &value, SVCDebug::baseAddress + address, 1)))
		menu->SetErrorFlag();
}

void Process::Read64(u32 address, u64 &value) {
	if(R_FAILED(svcReadDebugProcessMemory(&value, SVCDebug::debugHandle, SVCDebug::baseAddress + address, 8)))
		menu->SetErrorFlag();
}

void Process::Read32(u32 address, u32 &value) {
	if(R_FAILED(svcReadDebugProcessMemory(&value, SVCDebug::debugHandle, SVCDebug::baseAddress + address, 4)))
		menu->SetErrorFlag();
}

void Process::Read16(u32 address, u16 &value) {
	if(R_FAILED(svcReadDebugProcessMemory(&value, SVCDebug::debugHandle, SVCDebug::baseAddress + address, 2)))
		menu->SetErrorFlag();
}

void Process::Read8(u32 address, u8 &value) {
	if(R_FAILED(svcReadDebugProcessMemory(&value, SVCDebug::debugHandle, SVCDebug::baseAddress + address, 1)))
		menu->SetErrorFlag();
}