#ifndef MENUENTRY_HPP
#define MENUENTRY_HPP

#include <iostream>

#include <vector>

#include <switch.h>

class MenuEntry {
	using FuncPointer = void(*)(MenuEntry*);
	using MenuFuncPointer = void(*)();
	public:
		MenuEntry(const std::string &name, FuncPointer function);
		MenuEntry(const std::string &name, FuncPointer function, MenuFuncPointer menuFunction);

		void ChangeState();
		void Enable();
		void Disable();
		void SetWasActivatedStatus();
		void RemoveWasActivatedStatus();
		bool IsActivated();
		bool WasActivated();
	private:
		bool isActivated;
		bool wasActivated;
		std::string entryName;
		FuncPointer entryFunction;
		MenuFuncPointer entryMenuFunction;

		friend class CMenu;
		// friend class MenuFolder;
};

#endif // MENUENTRY_HPP