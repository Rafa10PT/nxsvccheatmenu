#include "CMenu.hpp"
#include "ARHandler.hpp"
#include "SVCDebug.hpp"

// using std::vector<std::string> = StringVector;

CMenu::CMenu(const std::string &name) {
	consoleClear();

	std::cout << name << std::endl;

	MemAccessFail = false;
}

bool CMenu::TryLoadConfig() {
	puts("Is it the first time executing this menu on the current game's boot?\n \
		Y: Yes.\n \
		X: No.\n");

	while(true) {
		hidScanInput();
		u32 kDown = hidKeysDown(CONTROLLER_P1_AUTO);

		if(kDown & KEY_Y) {
			SetEntriesFalse(&rootfolder);
			return true;
		}

		if(kDown & KEY_X) {
			std::ifstream entryfs("sdmc:/switch/NXSVCCheatMenu/config.bin", std::ios::binary);

			if(!entryfs.is_open()) {
				SetEntriesFalse(&rootfolder);
				return true;
			}

			if(entryfs.get() != 2)
				return false;
			
			return LoadEntries(&rootfolder, entryfs);
		}
	}
}

bool CMenu::SaveConfig() {
	std::ofstream entryfs("sdmc:/switch/NXSVCCheatMenu/config.bin", std::ios::binary | std::ios::trunc);

	if(!entryfs.is_open())
		return false;

	SaveEntries(&rootfolder, entryfs);

	return true;
}

void CMenu::operator += (MenuEntry *entry) {
	rootfolder.folderEntries.push_back(entry);
}

void CMenu::operator += (MenuFolder *folder) {
	rootfolder.subfolders.push_back(folder);
}

void CMenu::SetPatches() {
	std::vector<MenuFolder *> previousFolders;
	MenuFolder *currentFolder = &rootfolder;
	u32 cursorPosition = 0;

	while(true) {
		consoleClear();

		puts("Navigate through the menu with DPadUp and DPadDown.\n\
			Press A to enable/disable a cheat or to open a folder.\n\
			Press B to return from a folder.\n\
			Press Y to execute the configurable menu function (CFG) if it exists.\n\
			Press X to load codes from a TXT file if it exists.\n\
			Press ZL and ZR when you are done.");

		DisplayMenu(currentFolder, cursorPosition);
		if(MenuKeyInteract(currentFolder, previousFolders, currentFolder->subfolders.size() + currentFolder->folderEntries.size() - 1, cursorPosition) == (KEY_ZL | KEY_ZR)) return;
	}
}

bool CMenu::ApplyPatches() {
	return ExecuteEntryFunctions(&rootfolder);
}

void CMenu::DisplayMenu(MenuFolder *folder, unsigned int cursorPosition) {
	for(unsigned int i = 0; i < folder->subfolders.size(); ++i) {
		if(i == cursorPosition) putc('>', stdout);
		std::cout << " Folder: " << folder->subfolders[i]->folderName << std::endl;
	}

	u32 entryOffset = folder->subfolders.size();
	for(unsigned int i = 0; i < folder->folderEntries.size(); ++i) {
		if(i + entryOffset == cursorPosition) putc('>', stdout);
		if(folder->folderEntries[i]->entryMenuFunction != nullptr) std::cout << " CFG";
		std::cout << " [";
		if(folder->folderEntries[i]->IsActivated()) putc('*', stdout);
		else putc(' ', stdout);
		std::cout << "] Entry: " << folder->folderEntries[i]->entryName << std::endl;
	}
}

bool CMenu::TryLoadTXTConfig(Code codes[], std::fstream &entryfs, u32 boundary) {
	puts("Is it the first time executing this menu on the current game's boot?\n \
		Y: Yes.\n \
		X: No.\n");

	while(true) {
		hidScanInput();
		u32 kDown = hidKeysDown(CONTROLLER_P1_AUTO);

		if(kDown & KEY_Y)
			return true;

		if(kDown & KEY_X) {
			std::ifstream entryfs("sdmc:/switch/NXSVCCheatMenu/ar_config.bin", std::ios::binary);

			if(!entryfs.is_open())
				return true;
			
			return TXTLoadEntries(codes, entryfs, boundary);
		}
	}
}

void CMenu::SaveTXTEntries(Code codes[], std::fstream &entryfs, u32 boundary) {
	for(unsigned int i = 0; i <= boundary; ++i)
		entryfs.put(codes[i].isActivated);
}

bool CMenu::SaveTXTConfig(Code codes[], std::fstream &entryfs, u32 boundary) {
	if(!entryfs.is_open())
		return false;

	SaveTXTEntries(codes, entryfs, boundary);

	return true;
}

void CMenu::DisplayTXTEntriesMenu(Code codes[], u32 boundary, u32 cursorPosition) {
	for(unsigned int i = 0; i <= boundary; ++i) {
		if(i == cursorPosition) putc('>', stdout);
		std::cout << " [";
		if(codes[i].isActivated == true) putc('*', stdout);
		else putc(' ', stdout);
		std::cout << "] Entry: " << (codes[i].codeName.empty() ? "Unnamed" : codes[i].codeName) << std::endl;
	}
}

void CMenu::ExecuteAR(Code codes[], u32 boundary) {
	for(unsigned int i = 0; i <= boundary; ++i) {
		if(!codes[i].isActivated) continue;

		for(int j = 0; j < codes[i].currentValuePos;) {
			u8 codetype = codes[i].instructions[j] >> 28;
			if(codetype == 0xE) {
				u32 address = codes[i].instructions[j++] & 0xFFFFFFF;
				u32 byteAmount = codes[i].instructions[j++];
				u32 wordAmount = byteAmount >> 2;
				/* 
				 * If you have a pattern that doesn't end in 4, this subtraction won't be 0.
				 * EA000000 00000005  
				 * 01010101 00000001
				 * Writes 5 bytes. 5 >> 2 == 1, but we have 2 words 01010101 and 00000001.
				 * Hence, we need to check if the value is higher than the 32 bits offset before (in this case, 4).
				 * 2 & ~3 == 0 and 2 - 0 == 2, so we need to add 1 more. Then, we have the word amount.
				 */
				/*
				 * 0x4 and 0xC amounts.
				 * EA000000 0000000C
				 * 12345678 12345678
				 * 12345678 00000000 // They have 00000000 afterwards.
				 * 0xXXXXXXX4 & 7 == 4 and 0xXXXXXXXC == 4.
				 */
				if(((byteAmount - (byteAmount & ~3)) != 0 || (byteAmount & 7) == 4)) ++wordAmount;
				/*
				 * codes[i].currentValuePos = 4
				 * Not valid.
				 * EA000000 00000010 => Overflow
				 * AAAAAAAA AAAAAAAA
				 */
				if(j + wordAmount > codes[i].currentValuePos) break;

				svcWriteDebugProcessMemory(SVCDebug::debugHandle, &codes[i].instructions[j], SVCDebug::baseAddress + address, byteAmount);
				j += wordAmount;
				continue;
			}

			if(codetype == 0) {
				u32 address = codes[i].instructions[j++] & 0xFFFFFFF;
				u32 value = codes[i].instructions[j];

				svcWriteDebugProcessMemory(SVCDebug::debugHandle, &value, SVCDebug::baseAddress + address, sizeof(u32));
			}
			++j;
		}
	}
}

void CMenu::SetTXTEntriesPatches(Code codes[], u32 boundary) {
	std::fstream entryfs("sdmc:/switch/NXSVCCheatMenu/ar_config.bin", std::ios::in | std::ios::out | std::ios::ate | std::ios::binary);
	entryfs.seekp(0);

	if(!TryLoadTXTConfig(codes, entryfs, boundary)) {
		puts("The ar_config.bin file will be deleted, beacause it was invalid.\r\nPress A to delete the file and continue or B to exit.");

		while(true) {
			hidScanInput();
			u32 kDown = hidKeysDown(CONTROLLER_P1_AUTO);

			if(kDown & KEY_B)
				return;

			if(kDown & KEY_A) {
				if(remove("sdmc:/switch/NXSVCCheatMenu/config.bin") != 0)
					puts("Error deleting file.");
			}
		}
	}

	u32 cursorPosition = 0;

	while(true) {
		consoleClear();

		puts("Navigate through the menu with DPadUp and DPadDown.\n\
			Press A to enable/disable a cheat or to open a folder.\n\
			Press B to return from a folder.\n\
			Press ZL and ZR when you are done.");

		DisplayTXTEntriesMenu(codes, boundary, cursorPosition);
		if(TXTMenuKeyInteract(codes, boundary, cursorPosition) == (KEY_ZL | KEY_ZR)) break;
	}

	ExecuteAR(codes, boundary);

	SaveTXTConfig(codes, entryfs, boundary);
}

void CMenu::CreateTXTEntries(Code codes[]) {
	ARHandler handler("sdmc:/switch/NXSVCCheatMenu/cheats.txt");

	consoleClear();

	for(unsigned int i = 0; ; ++i) {
		static bool ret = false;
		if(!handler.GetCodeName(codes[i].codeName)) --i;
			

		if(handler.WasEOFReached()) {
			SetTXTEntriesPatches(codes, i);
			return;
		}

		ret = true; // This is only reached if it could load one entry name.

		handler.SeekCode(&codes[i]);

		if(handler.WasEOFReached()) {
			SetTXTEntriesPatches(codes, i); // The i variable here is the boundary limit for code indexes. So, the number of codes is i + 1.
			return;
		}
	}
}

u32 CMenu::TXTMenuKeyInteract(Code codes[], u32 boundary, u32 &cursorPosition) {
	while(true) {
		hidScanInput();
		u32 kDown = hidKeysDown(CONTROLLER_P1_AUTO);

		if(kDown & KEY_A) {
			codes[cursorPosition].isActivated = !codes[cursorPosition].isActivated;

			return KEY_A;
		}

		/*
		if(kDown & KEY_B) {
			Folders will be added soon...
		}
		*/

		if(kDown & KEY_DUP) {
			if(cursorPosition > 0) --cursorPosition;
			return KEY_DUP;
		}

		if(kDown & KEY_DDOWN) {
			if(cursorPosition < boundary) ++cursorPosition;
			return KEY_DDOWN;
		}

		else if(kDown & (KEY_ZL | KEY_ZR))
			return KEY_ZL | KEY_ZR;
	}
}

bool CMenu::TXTLoadEntries(Code codes[], std::ifstream &entryfs, u32 boundary) {
	for(unsigned int i = 0; i <= boundary; ++i) {
		u8 value = entryfs.get();

		if(value == 1) codes[i].isActivated = true;
		else if(!value) codes[i].isActivated = false;
		else return false;
	}

	return true;
}

u32 CMenu::MenuKeyInteract(MenuFolder *&currentFolder, std::vector<MenuFolder *> &previousFolders, u32 boundary, u32 &cursorPosition) {
	while(true) {
		hidScanInput();
		u32 kDown = hidKeysDown(CONTROLLER_P1_AUTO);

		u32 subfolderAmount = currentFolder->subfolders.size();
		if(kDown & KEY_A) {
			if(cursorPosition >= subfolderAmount) currentFolder->folderEntries[cursorPosition - subfolderAmount]->ChangeState();

			else {
				previousFolders.push_back(currentFolder);

				currentFolder = currentFolder->subfolders[cursorPosition];
			}

			return KEY_A;
		}

		u32 previousFolderAmount = previousFolders.size();
		if(kDown & KEY_B) {
			if(previousFolderAmount > 0) {
				currentFolder = previousFolders[previousFolderAmount - 1];
				previousFolders.erase(previousFolders.begin() + previousFolderAmount - 1);
			}
			return KEY_B;
		}

		if(kDown & KEY_X) {
			Code *codes = new Code[1000];

			CreateTXTEntries(codes);

			return KEY_X;
		}

		if(kDown & KEY_Y) {
			MenuEntry::MenuFuncPointer p = currentFolder->folderEntries[cursorPosition - subfolderAmount]->entryMenuFunction;
			if(p != nullptr) {
				consoleClear();
				(*p)();
			}
			return KEY_Y;
		}

		if(kDown & KEY_DUP) {
			if(cursorPosition > 0) --cursorPosition;
			return KEY_DUP;
		}

		if(kDown & KEY_DDOWN) {
			if(cursorPosition < boundary) ++cursorPosition;
			return KEY_DDOWN;
		}

		else if(kDown & (KEY_ZL | KEY_ZR))
			return KEY_ZL | KEY_ZR;
	}
}

bool CMenu::LoadEntries(MenuFolder *folder, std::ifstream &entryfs) {
	for(unsigned int i = 0; i < folder->subfolders.size(); ++i) {
		if(entryfs.get() != 2) return false;
		if(!LoadEntries(folder->subfolders[i], entryfs)) return false;
	}

	for(unsigned int i = 0; i < folder->folderEntries.size(); ++i) {
		u8 value = entryfs.get();

		if(value == 1) {
			folder->folderEntries[i]->Enable();
			folder->folderEntries[i]->SetWasActivatedStatus();
		}
		else if(!value) folder->folderEntries[i]->Disable();
		else return false;
	}

	if(entryfs.get() == 3)
		return true;

	return false;
}

void CMenu::SetEntriesFalse(MenuFolder *folder) {
	for(unsigned int i = 0; i < folder->folderEntries.size(); ++i) {
		folder->folderEntries[i]->Disable();
		folder->folderEntries[i]->RemoveWasActivatedStatus();
	}

	for(unsigned int i = 0; i < folder->subfolders.size(); ++i)
		SetEntriesFalse(folder->subfolders[i]);
}

void CMenu::SaveEntries(MenuFolder *folder, std::ofstream &entryfs) {
	entryfs.put(2);

	for(unsigned int i = 0; i < folder->subfolders.size(); ++i)
		SaveEntries(folder->subfolders[i], entryfs);

	for(unsigned int i = 0; i < folder->folderEntries.size(); ++i)
		entryfs.put((u8)folder->folderEntries[i]->IsActivated());

	entryfs.put(3);
}

bool CMenu::ExecuteEntryFunctions(MenuFolder *folder) {
	for(unsigned int i = 0; i < folder->folderEntries.size(); ++i)
		if(folder->folderEntries[i]->entryFunction != nullptr && (folder->folderEntries[i]->IsActivated() || folder->folderEntries[i]->WasActivated())) {
			(*folder->folderEntries[i]->entryFunction)(folder->folderEntries[i]);
			if(MemAccessFail) return false; 
		}

	for(unsigned int i = 0; i < folder->subfolders.size(); ++i)
		if(!ExecuteEntryFunctions(folder->subfolders[i])) return false;

	return true;
}

void CMenu::SetErrorFlag() {
	MemAccessFail = true;
}