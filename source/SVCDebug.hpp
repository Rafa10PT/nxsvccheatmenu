#ifndef SVCDEBUG_HPP
#define SVCDEBUG_HPP

#include <switch.h>

#define MAX_PROCESS_NUMBER 300

class SVCDebug {
	public:
		static u32 MenuKeyInteract();
		static void DisplayMenu();
		static int GetProcessListInfo(int processLimit);
		static void ErrorOut(const char *s, Result r);
		static bool AttachFindMemreg();
	private:
		static Handle debugHandle;
		static u64 baseAddress;
		static u32 currentProcess;
		struct ProcessInfo {
			static u32 totalProcesses;
			static u64 *pidList;
			static u64 *tidList;
		};
		friend class Process;
		friend class CMenu;
};

#endif // SVCDEBUG_HPP