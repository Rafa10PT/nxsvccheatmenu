#include "ARHandler.hpp"

ARHandler::ARHandler(const char path[]) {
	arFile.open(path, std::ios::in | std::ios::out | std::ios::ate);

	if(!arFile.is_open()) {
		// TODO.
	}

	size = arFile.tellp();
	arFile.seekp(0);

	wasEOFReached = false;
	returnEnterDetected = false;
	checkLine = false;
}

bool ARHandler::GetCodeName(std::string &codeName) {
	while(true) {
		std::string nameLine = "";
		u32 nameLinePos = arFile.tellp();

		if(!nameLinePos) checkLine = true; // Start of file.

		GetTXTLine(nameLine);

		u32 nextLinePos = arFile.tellp();

		if(arFile.eof()) {
			arFile.clear();
			wasEOFReached = true;
		}

		if(nameLine.empty()) {
			if(!checkLine) checkLine = true;
			else DeleteChars(nameLinePos, nextLinePos - nameLinePos);
			continue;
		}

		/*
		else if(!checkLine) {
			u32 a = 0;
			PutCharAtPlace('\r', nextLinePos++, a);
			PutCharAtPlace('\n', nextLinePos++, a);
			checkLine = true;
		}
		*/

		u32 lowerbound = 0, upperbound = 0;
		bool wasLeftBracketFound = false, wasRightBracketFound = false;

		for(unsigned int i = 0; i < nameLine.length(); ++i) {
			if(nameLine[i] == '[') {
				wasLeftBracketFound = true;
				lowerbound = i;
				break;
			}
		}

		for(int i = nameLine.length() - 1; i >= 0; --i) {
			if(nameLine[i] == ']') {
				wasRightBracketFound = true;
				upperbound = i;

				if(!wasLeftBracketFound) {
					PutCharAtPlace('[', nameLinePos, nextLinePos);
					lowerbound = nameLinePos;
				}

				break;
			}


			if(!i && wasLeftBracketFound) {
				upperbound = nameLine.length();
				PutCharAtPlace(']', nameLinePos + upperbound++, nextLinePos);
			}
		}

		if(!wasLeftBracketFound && !wasRightBracketFound) {
			DeleteChars(nameLinePos, nameLine.length() + 1 + returnEnterDetected);
			if(wasEOFReached) return false;
		}

		else {
			if((wasLeftBracketFound || wasRightBracketFound) && !checkLine) {
					PutCharAtPlace('\r', nameLinePos++, nextLinePos);
					PutCharAtPlace('\n', nameLinePos++, nextLinePos);
					arFile.seekp(nextLinePos);
			}
			for(unsigned int i = lowerbound + 1; i < upperbound; ++i)
				codeName += nameLine[i];
			break;
		}
/*
	std::string s;
	arFile.seekp(0);
	arFile.seekg(0);
	getline(arFile, s);
	std::cout << codeName << std::endl;
	std::cout << s << std::endl;
	while(true);
*/
	}
	checkLine = false;
	return true;
}

void ARHandler::SeekCode(Code *code) {
	while(true) {
		std::string codeLine;
		u32 codeLinePos = arFile.tellp();
		bool wasReturnEnterDetected = returnEnterDetected;
		GetTXTLine(codeLine);

		if(arFile.eof()) {
			arFile.clear();
			wasEOFReached = true;
		}

		if(codeLine.empty()) {
			checkLine = true;
			return;
		}

		static const std::string validCharacters = "0123456789ABCDEF"; // Valid characters.
		u32 backup = 0;
		u32 value = 0;
		for(unsigned int i = 0, pos = 0, codeLineLen = codeLine.length(); i < codeLineLen; ++i) {
			char c = codeLine[i];
			if(c == ' ' || c == '\t') continue;

			if(c == '[' || c == ']') {
				arFile.seekg(codeLinePos);
				return;
			}

			if(codeLine.length() < 16) { // 16+ characters per line.
				if(i == codeLineLen - 1) {
					DeleteChars(codeLinePos, codeLine.length() + 1 + wasReturnEnterDetected); // Delete line.
					return;	
				}
				else continue;
			}

			bool isCodeValid = false;
			for(unsigned int j = 0, checkCharLen = validCharacters.length(); j < checkCharLen; ++j) {
				if(c == validCharacters[j]) {
					isCodeValid = true;
					value |= (c > 0x39 ? c - 0x37 : c - 0x30) << ((7 - pos) << 2);
					break;
				}
			}

			if(!isCodeValid) {
				arFile.seekp(codeLinePos);
				DeleteChars(codeLinePos, codeLine.length() + 1 + wasReturnEnterDetected);
				return;
			}

			++pos;
			if(pos == 8) {
				backup = value;
				value = 0; // To let it hold the next value.
			}

			else if(pos == 16) {
				code->instructions[code->currentValuePos++] = backup;
				code->instructions[code->currentValuePos++] = value;
			}
		}

		if(wasEOFReached) return;
	}
}

bool ARHandler::GetU32ValsAR(u32 values[], int &pos) {
	std::string codeLine;
	std::getline(arFile, codeLine);

	if(codeLine.length() != 17) return false; // 17 characters per line.
	char c;
	for(int i = 0, j = 0; i < codeLine.length(); ++i, ++j) {
		if(i == 8) { // Position 8 must be spacebar.
			if(c != ' ') return false;
			else {
				++pos;
				j = -1;
				continue;
			}
		}

		static const std::string validCharacters = "0123456789ABCDEF"; // Valid characters.

		bool isCodeValid = false;

		for(unsigned int k = 0; k < validCharacters.length(); ++k) {
			if(c == validCharacters[k]) {
				isCodeValid = true;
				// We have 2 u32 values for each line in the matrix.
				values[pos] |= (k > 9 ? c - 0x37 : c - 0x30) << ((8 - j) << 2);
				break;
			}
		}

		if (!isCodeValid) return false;
	}
    
    return true;
}

bool ARHandler::WasEOFReached() {
	return wasEOFReached;
}

void ARHandler::ShiftFileCharsRight(u32 shift) {
	u32 posBackup = arFile.tellp();

	for(unsigned int i = size; i > posBackup; --i) {
		arFile.seekg(i - 1);

		char c = arFile.get();
		arFile.seekp(i + shift - 1);
		arFile << c;
	}

	arFile.seekp(posBackup);
}

void ARHandler::ShiftFileCharsLeft(u32 shift) {
	u32 posBackup = arFile.tellp();

	for(unsigned int i = posBackup; i < size; ++i) { 
		arFile.seekg(i);

		char c = arFile.get();
		arFile.seekp(i - shift);
		arFile << c;
	}

	arFile.seekp(posBackup - shift);
}

void ARHandler::PutCharAtPlace(char c, u32 place, u32 &currentPlace) {
	if(size == place) {
		arFile.seekp(size);
		arFile << c;
	}

	else {
		arFile.seekp(place);

		ShiftFileCharsRight(1);

		arFile << c;

		arFile.seekp(++currentPlace);
	}

	++size;
}

void ARHandler::GetTXTLine(std::string &nameLine) {
	// New lines are only '\n', '\r' or "\r\n". You can't have a new line sequences of '\r' or '\n' are each one a new line.
	// EOF can also finish a line, but also finishes a file.
	int c;
	while((c = arFile.get()) != '\r' && c != '\n' && c != EOF) nameLine += c;
	if(c == '\r') {
		if(arFile.get() != '\n') {
			arFile.seekg((unsigned int)arFile.tellg() - 1);
			returnEnterDetected = false;
		}
		else returnEnterDetected = true;
	}
}

void ARHandler::DeleteChars(int pos, int n) {
	ShiftFileCharsLeft(n);

	FsFileSystem sdmc = {0};
	FsFile out;

    while (R_FAILED(fsMountSdcard(&sdmc))) {
        svcSleepThread(1000ULL);
    }

    fsFsOpenFile(&sdmc, "/switch/NXSVCCheatMenu/cheats.txt", FS_OPEN_WRITE, &out);
    fsFileSetSize(&out, size -= n); // Size is updated and parsed.
    fsFsClose(&sdmc);
    fsFileClose(&out);
}