#ifndef MENUFOLDER_HPP
#define MENUFOLDER_HPP

#include <algorithm>

#include "MenuEntry.hpp"

class MenuFolder {
	public:
		std::string folderName;

		std::vector<MenuFolder *> subfolders;
		std::vector<MenuEntry *> folderEntries;

		MenuFolder();
		MenuFolder(const std::string &name, const std::vector<MenuEntry *> &entries);

		void operator += (MenuEntry *entry);

		void operator -= (MenuEntry *entry);

		void operator += (MenuFolder *folder);

		void operator -= (MenuFolder *folder);
};

#endif