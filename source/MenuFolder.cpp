#include "MenuFolder.hpp"



MenuFolder::MenuFolder() {}

MenuFolder::MenuFolder(const std::string &name, const std::vector<MenuEntry *> &entries) : folderName{name}, folderEntries{entries} {}

void MenuFolder::operator += (MenuEntry *entry) {
	folderEntries.push_back(entry);
}

void MenuFolder::operator -= (MenuEntry *entry) {
	std::vector<MenuEntry *>::iterator i = std::find(folderEntries.begin(), folderEntries.end(), entry);
	if(i != folderEntries.end()) {
		folderEntries.erase(i);
	}
}

void MenuFolder::operator += (MenuFolder *folder) {
	subfolders.push_back(folder);
}

void MenuFolder::operator -= (MenuFolder *folder) {
	std::vector<MenuFolder *>::iterator i = std::find(subfolders.begin(), subfolders.end(), folder);
	if(i != subfolders.end()) {
		subfolders.erase(i);
	}
}