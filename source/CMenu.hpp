#ifndef CMENU_HPP
#define CMENU_HPP

#include "MenuFolder.hpp"

#include <fstream>

#include <switch.h>

typedef struct {
	std::string codeName{""};
	u32 *instructions{new u32[4000]};
	u32 currentValuePos{0};
	bool isActivated{false};
} Code;

class CMenu {
	public:
		CMenu(const std::string &name);
		bool TryLoadConfig();
		bool SaveConfig();

		void operator += (MenuEntry *entry);
		void operator += (MenuFolder *folder);

		void SetPatches();
		bool ApplyPatches();
		void DisplayMenu(MenuFolder *folder, unsigned int cursorPosition);
		bool TryLoadTXTConfig(Code codes[], std::fstream &entryfs, u32 boundary);
		void SaveTXTEntries(Code codes[], std::fstream &entryfs, u32 boundary);
		bool SaveTXTConfig(Code codes[], std::fstream &entryfs, u32 boundary);
		void DisplayTXTEntriesMenu(Code codes[], u32 boundary, u32 cursorPosition);
		void ExecuteAR(Code codes[], u32 boundary);
		void SetTXTEntriesPatches(Code codes[], u32 boundary);
		void CreateTXTEntries(Code codes[]);
		u32 TXTMenuKeyInteract(Code codes[], u32 boundary, u32 &cursorPosition);
		bool TXTLoadEntries(Code codes[], std::ifstream &entryfs, u32 boundary);
		u32 MenuKeyInteract(MenuFolder *&currentFolder, std::vector<MenuFolder *> &previousFolders, u32 boundary, u32 &cursorPosition);

		bool LoadEntries(MenuFolder *folder, std::ifstream &entryfs);
		void SetEntriesFalse(MenuFolder *folder);
		void DetermineExecution();
		void SaveEntries(MenuFolder *folder, std::ofstream &entryfs);
		bool ExecuteEntryFunctions(MenuFolder *folder);

		void SetErrorFlag();
	private:
		MenuFolder rootfolder;
		bool MemAccessFail;
};

#endif // CMENU_HPP