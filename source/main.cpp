#include "CMenu.hpp"
#include "SVCDebug.hpp"
#include "cheats.hpp"

void CreateMenu(CMenu &menu) {
	Process::menu = &menu;

	menu += new MenuFolder("VR and Pts",
	{
		new MenuEntry("VR Limiter", vr_limiter, vr_limiter_setter)
	});
}

void BPressExit() {
	while(true) {
		hidScanInput();
		u32 kDown = hidKeysDown(CONTROLLER_P1_AUTO);
		if(kDown & KEY_B)
			gfxExit();
	}
}

int main() {
	gfxInitDefault();
	consoleInit(NULL);

	SVCDebug::GetProcessListInfo(MAX_PROCESS_NUMBER);
	SVCDebug::DisplayMenu();

	if(SVCDebug::MenuKeyInteract() == KEY_B) {
		gfxExit();
		return 0;
	}

	if(!SVCDebug::AttachFindMemreg()) {
		puts("Press B to exit.");

		BPressExit();

		return 1;
	}

	CMenu *menu = new CMenu("Mario Kart 8 Deluxe");

	CreateMenu(*menu);

	if(!menu->TryLoadConfig()) {
		puts("The config.bin file was deleted, beacause it was invalid.\r\nPress A to delete the file and continue or B to exit.");

		while(true) {
			hidScanInput();
			u32 kDown = hidKeysDown(CONTROLLER_P1_AUTO);

			static int error = 2;

			if(kDown & KEY_B) {
				gfxExit();
				return error;
			}

			if(kDown & KEY_A) {
				if(remove("sdmc:/switch/NXSVCCheatMenu/config.bin") != 0) {
					puts("Error deleting file.");
					error = 3;
				}
			}
		}
	}

	menu->SetPatches();

	if(!menu->ApplyPatches()) {
		puts("Neither the entries had their cheats executed nor were their configuration saved to config.bin.");

		BPressExit();

		return 4;
	}

	// Save configuration.
	if(menu->SaveConfig() == false) {
		puts("Could not save to the config.bin file.");

		BPressExit();

		return 5;
	}

	puts("Patches written and saved to config.bin. Press B to exit.");

	BPressExit();
	return 0;
}