#ifndef AR_HANDLER_HPP
#define AR_HANDLER_HPP

#include "CMenu.hpp"

#include <iostream>
#include <fstream>
#include <vector>

#include <switch.h>

class ARHandler {
	public:
		ARHandler(const char path[]);
		bool GetCodeName(std::string &codeName);
		void SeekCode(Code *code);
		bool GetU32ValsAR(u32 values[], int &pos);
		void DiscardLine();
		bool WasEOFReached();
	private:
		void ShiftFileCharsRight(u32 shift);
		void ShiftFileCharsLeft(u32 shift);
		void GetTXTLine(std::string &nameLine);
		void PutCharAtPlace(char c, u32 place, u32 &currentPlace);
		void DeleteChars(int pos, int n);
		std::fstream arFile;
		u32 size;
		bool wasEOFReached;
		bool returnEnterDetected;
		bool checkLine;
};

#endif // AR_HANDLER_HPP